package de.htwg.se.stratego.model

case class Player(name: String) {
   override def toString:String = name
}

